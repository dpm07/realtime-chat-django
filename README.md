# Real time Chat App - Django Channels Tutorial

This repo is a live chat of a tutorial found in the Django Channels documentation. The goal is to familiarize myself with WebSockets and the Channels library, to work together with Django and in the future to implement real-time protocols like WebSockets and others.

https://channels.readthedocs.io/en/stable/tutorial/index.html